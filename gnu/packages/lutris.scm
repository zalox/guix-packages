(define-module (lutris)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages wine)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages pkg-config)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system python)
  #:use-module (guix licenses))

(define-public lutris
  (package
    (name "lutris")
    (version "0.5.3")
    (home-page "https://lutris.net/")
    (synopsis "Play all your games on Linux")
    (description "Lutris is an Open Source gaming platform for Linux. It installs and launches games so you can start playing without the hassle of setting up your games. Get your games from GOG, Steam, Battle.net, Origin, Uplay and many other sources running on any Linux powered gaming machine.")
    (license gpl3)
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://lutris.net/releases/lutris_" version ".tar.xz"))
              (sha256
               (base32
                "1bq5bfvadlccifim324q00ygmkil3xq763vl5vfa4pxli6kirf1d"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-before 'check
             'set-up-home
           (lambda* _
             (setenv "HOME" "/tmp")))
         (add-after 'install 'wrap-program
           (lambda* (#:key outputs inputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (input-path (lambda (lib path)
                                  (string-append (assoc-ref inputs lib) path)))
                    (libraries '("mesa" "glu" "libdrm" "webkitgtk"))
                    (libs
                     (map (lambda (lib) (input-path lib "/lib"))
                          libraries))
                    (gi-typelib-path (getenv "GI_TYPELIB_PATH")))
               (wrap-program (string-append out "/bin/lutris")
                 `("GI_TYPELIB_PATH" = (,gi-typelib-path))
                 `("LD_LIBRARY_PATH" ":" prefix ,libs))
               #t))))))
    (propagated-inputs
     `(("python", python-wrapper)
       ("gobject-introspection", gobject-introspection)
       ("python-pygobject", python-pygobject)
       ("python-requests", python-requests)
       ("python-pillow", python-pillow)
       ("python-yaml", python-pyyaml)
       ("python-evdev", python-evdev)
       ("libnotify", libnotify)
       ("gnome", gnome)
       ("gnome-desktop", gnome-desktop)
       ("psmisc", psmisc)
       ("p7zip", p7zip)
       ("curl", curl)
       ("cabextract", cabextract)
       ("xrandr", xrandr)
       ("glib", glib)
       ("gtk+", gtk+)
       ("wine", wine)
       ("wine-staging", wine-staging)
       ("wine64", wine64)
       ("wine64-staging", wine64-staging)
       ("mesa", mesa)
       ("glu", glu)
       ("webkitgtk", webkitgtk)
       ("libdrm", libdrm)
       ))
    (native-inputs
     `(("pkg-config" ,pkg-config)))))

lutris
