(define-module (python-evdev))

(define-public python-evdev
  (package
    (name "python-evdev")
    (version "1.2.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "evdev" version))
       (sha256
        (base32
         "0rh1228vlgmmm2hysy47ksq458ais8qvi0d9jis359dlwldmwgxh"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-after 'check
             'set-home-directory (lambda _ (setenv "HOME" "/tmp") #t))
         (replace 'build
           (lambda* (#:key inputs #:allow-other-keys)
             (invoke
              "python"
              "setup.py"
              "build"
              "build_ecodes"
              "--evdev-headers"
              (string-append
               (string-append
                (assoc-ref inputs "kernel-headers") "/include/linux/input.h:")
               (string-append
                (assoc-ref inputs "kernel-headers") "/include/linux/input-event-codes.h"))
              "build_ext"
              "--include-dirs"
              (string-append (assoc-ref inputs "linux-headers") "/include"))))
         (replace 'check (lambda _  #t)))))

    (home-page
     "https://github.com/gvalkov/python-evdev")
    (synopsis
     "Bindings to the Linux input handling subsystem")
    (description
     "Bindings to the Linux input handling subsystem")
    (license #f)

    )
  )
